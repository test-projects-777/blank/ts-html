const HtmlWebpackPlugin = require('html-webpack-plugin')
const CircularDependencyPlugin = require("circular-dependency-plugin")
const path = require('path')
const SRC_DIR = path.resolve(__dirname, 'src')
const DIST_DIR = path.resolve(__dirname, 'dist')

const commonStyleLoaders = [
  'style-loader',
  {
    loader: 'css-loader',
    options: {
      // necessary to minify @import-ed files using cssnano
      importLoaders: 1,
    },
  },
  {
    loader: 'postcss-loader',
    options: {
      plugins: [
        require('autoprefixer'),
        require('cssnano'),
      ],
    },
  },
];

module.exports = {
  entry: path.join(SRC_DIR, 'index.ts'),
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: [...commonStyleLoaders]
      },
      {
        test: /\.less/,
        loader: [...commonStyleLoaders, 'less-loader'],
      },
      // {
      //   test: /\.scss$/,
      //   loader: [...commonStyleLoaders, 'sass-loader'],
      // },
      {
        test: /\.(woff|woff2|ttf|svg|eot)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10, // при 10000 инлайнит шрифты в base64
              name: '[name].[ext]', // [path][name].[hash].[ext]
            }
          }
        ]
      }
    ]
  },
  output: {
    path: DIST_DIR,
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: DIST_DIR
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.join(SRC_DIR, 'index.html')
    }),
    new CircularDependencyPlugin({
      exclude: /.js|node_modules/,
      failOnError: true,
    }),
  ],
  resolve: {
    extensions: ['.js', '.ts']
  },
};
